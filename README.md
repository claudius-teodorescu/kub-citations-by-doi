# kub-citations-by-doi



## Scope

Web component...

Attributes: @cited-doi, @period, @databases (others than the default ones, namely CrossRef, OpenCitations).

It has to show in which database a citation was found.
It has to show a message if no citation was found.
